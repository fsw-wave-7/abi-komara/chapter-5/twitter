const express = require("express")
const path = require("path")
const bodyParser = require('body-parser'); 

const app = express()
const port = 3000

const jsonParser = bodyParser.json()
const Post = require("./controller/post.js");

const post = new Post

app.get("/", function(req, res) {
    res.json({
        "hello" : "world"
    })
})

    
app.get("/post", post.getPost)
app.post("/post", jsonParser, post.insertPost)
app.put("/post/:index", jsonParser, post.updatePost)
app.delete("./post/:index", post.deletePost)



app.listen(port, () => {console.log("Server berhasil dijalankan!!") })