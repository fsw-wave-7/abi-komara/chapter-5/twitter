const { json } = require("body-parser")
const { successResponse} = require("../helper/response.js")

class Post {
    constructor() {
        this.post = [{
            "text": "postingan pertama saya",
            "created_at": "2021-03-11 21:10:00",
            "username" : "jono"
        },
        {
            "text": "postingan kedua saya",
            "created_at": "2021-03-11 21:10:00",
            "username" : "abi"
        }
        ]
    }

    getPost = (req, res) => {
        // res.json({
        //     "ini": "getpost"
        // })

        // res.status(200), json({
        //     data: this.post,
        //     meta: {
        //         code: 200,
        //         message: "sukses mengambil data",
        //         total: this.post.length
        //     }
        // })
        
        successResponse(
            res, 
            200,
            this.post,
            { total: this.post.length}
        )

    }

    getDetailPost = (req, res) => {
        const index = req.params.index
        successResponse (
            res, 200, code, this.post[index]
            )

        // res.status(200),json({
        //     data: this.post[index],
        //     meta: {
        //         code: 200,
        //         message: "sukses mengambil data"
        //         }
        // })
        
    }

    insertPost = (req, res) => {
        const body = req.body

        const param = {
            "text" : body.text,
            "created_at": new Date(),
            "username": body.username
        }
        
        this.post.push(param)
        successResponse (
            res, 200, code, this.post[index]
            )
        // res.json({
        //     data: param,
        //     meta: {
        //         code: 201,
        //         message: "sukses mengambil data"
        //         }
        //     })
        }
    

    updatePost = (req, res) => {
       const index = req.params.index
       const body = req.body

       this.post[index].text = body.text
       this.post[index].username = body.username
       this.post[index].created_at = body.created_at
        
       successResponse(res, 200, this.post[index])
    //    res.status(200).json ({
    //        data: this.post[index],
    //        meta: {
    //            code: 200,
    //            message: "sukses mengubah data"
    //        }
    //    })
    }

    deletePost = (req, res) => {
        const index = req.params.index

        this.post.splice(index, 1)
        successResponse(res, 200, null)

        // res.status(200).json ({
        //     data: this.post[index],
        //     meta: {
        //         code: 200,
        //         message: "sukses mengubah data"
        //     }
        // })

    }

}
module.exports = Post